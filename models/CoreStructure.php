<?php

namespace yii2portal\structure\models;

use Yii;
use yii\helpers\Url;
use yii\web\GroupUrlRule;

/**
 * This is the model class for table "core_structure".
 *
 * @property integer $id
 * @property integer $pid
 * @property integer $level
 * @property integer $ord
 * @property string $title
 * @property string $alt
 * @property string $url
 * @property string $module
 * @property integer $redirect_id
 * @property string $redirect
 * @property string $olang
 */
class CoreStructure extends \yii\db\ActiveRecord
{

    private $_parents = null;

    /**
     * @var GroupUrlRule
     */
    protected $_urlRule;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'core_structure';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pid', 'level', 'ord', 'redirect_id'], 'integer'],
            [['ord', 'redirect'], 'required'],
            [['title', 'alt', 'url', 'module', 'redirect', 'olang'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'pid' => 'Pid',
            'level' => 'Level',
            'ord' => 'Ord',
            'title' => 'Title',
            'alt' => 'Alt',
            'url' => 'Url',
            'module' => 'Module',
            'redirect_id' => 'Redirect ID',
            'redirect' => 'Redirect',
            'olang' => 'Olang',
        ];
    }


    public function getParams()
    {
        return $this->hasMany(CoreStructureParams::className(), ['pid' => 'id']);
    }

    public function getUrlPath()
    {
        return Yii::$app->modules['structure']->getUrl($this->id);
    }

    public function getParent()
    {
        return $this->pid? Yii::$app->modules['structure']->getPage($this->pid): null;
    }

    public function getParents()
    {
        if($this->_parents === null) {
            $this->_parents = [];
            $allParents = function($page) use(&$allParents){
                if($page->parent){
                    $allParents($page->parent);
                    $this->_parents[] = $page->parent;
                }
            };
            $allParents($this);
            array_shift($this->_parents);
        }


        return $this->_parents;
    }

    public function getParam($type)
    {
        $params = $this->params;
        $param = array_filter($params, function ($param) use ($type) {
            return $type == $param->param;
        });

        return !empty($param) ? array_shift($param) : null;
    }

    public function getParamValue($type, $default = null)
    {
        $param = $this->getParam($type);
        return $param ? $param->value : $default;
    }


    public function getUrlRule()
    {
        return Yii::$app->modules['structure']->getUrlRule($this->id);
    }


    public function getRealModule()
    {
        $moduleName = $this->module;
        list($virtualModule) = explode('-', $moduleName);

        $moduleReal = $this->module;

        if (!isset(Yii::$app->modules[$moduleName]) && isset(Yii::$app->modules[$virtualModule])) {
            $moduleReal = $virtualModule;
        }

        return $moduleReal;
    }
}
