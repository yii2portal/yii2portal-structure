<?php

namespace yii2portal\structure\models;

use Yii;

/**
 * This is the model class for table "core_structure_params".
 *
 * @property string $id
 * @property string $pid
 * @property string $param
 * @property string $value
 * @property integer $separated
 */
class CoreStructureParams extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'core_structure_params';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'pid'], 'required'],
            [['pid', 'separated'], 'integer'],
            [['value'], 'string'],
            [['id', 'param'], 'string', 'max' => 255],
            [['id'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'pid' => 'Pid',
            'param' => 'Param',
            'value' => 'Value',
            'separated' => 'Separated',
        ];
    }
}
