<?php


namespace yii2portal\structure\components;

use yii2portal\core\components\Widget;
use yii\helpers\Html;
use yii\helpers\Url;
use yii2portal\media\models\Media;
use yii2portal\structure\models\CoreStructure;

class Meta extends Widget
{

    /**
     * @param $structure CoreStructure
     * @param $defaultImg Media
     */
    public function register($structure, $defaultImg = null)
    {

        $imagePath = null;
        $image = null;
        if($defaultImg && $defaultImg instanceof Media){
            $image = $defaultImg;
        }
        if($image){
            $imagePath = Url::to($image->srcUrl, true);
        }

        $description = '';
        $content = $structure->getParamValue('content');
        if ($content) {
            if (preg_match('#(<p[^>]*>.*?</p>)#uim', $content, $out)) {
                $description = strip_tags($out[0]);
            }
        }

        $this->view->registerMetaTag([
            'content' => Html::encode(strip_tags($description)),
            'name' => 'description'
        ], 'description');
        $this->view->registerMetaTag([
            'content' => Html::encode(strip_tags($structure->title)),
            'name' => 'title'
        ], 'title');
        $this->view->registerMetaTag([
            'content' => Html::encode(strip_tags($description)),
            'property' => 'og:description'
        ], 'og:description');
        $this->view->registerMetaTag([
            'content' => Html::encode(strip_tags($structure->title)),
            'property' => 'og:title'
        ], 'og:title');
        if($imagePath) {
            $this->view->registerMetaTag([
                'content' => $imagePath,
                'property' => 'og:image'
            ], 'og:image');
            $this->view->registerMetaTag([
                'content' => $imagePath,
                'name' => 'image_src'
            ], 'image_src');
            $this->view->registerMetaTag([
                'content' => $imagePath,
                'name' => 'twitter:image:src'
            ], 'twitter:image:src');
        }
        $this->view->registerMetaTag([
            'content' => Url::to($structure->urlPath, true),
            'property' => 'og:url'
        ], 'og:url');
        $this->view->registerMetaTag([
            'content' => 'article',
            'property' => 'og:type'
        ], 'og:type');
        $this->view->registerMetaTag([
            'content' => '24.kg',
            'property' => 'og:site_name'
        ], 'og:site_name');
        $this->view->registerMetaTag([
            'content' => 'https://www.facebook.com/www.24.kg',
            'property' => 'og:article:publisher'
        ], 'og:article:publisher');

        $this->view->registerMetaTag([
            'content' => Html::encode(strip_tags($description)),
            'name' => 'twitter:description'
        ], 'twitter:description');
        $this->view->registerMetaTag([
            'content' => 'summary_large_image',
            'name' => 'twitter:card'
        ], 'twitter:card');
        $this->view->registerMetaTag([
            'content' => '@_24_kg',
            'name' => 'twitter:site'
        ], 'twitter:site');
        $this->view->registerMetaTag([
            'content' => '249014600',
            'name' => 'twitter:site:id'
        ], 'twitter:site:id');
        $this->view->registerMetaTag([
            'content' => Html::encode(strip_tags($structure->title)),
            'name' => 'twitter:title'
        ], 'twitter:title');
    }

}