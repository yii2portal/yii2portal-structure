<?php

namespace yii2portal\structure\components\cplugin;

use yii2portal\cplugin\models\Cplugin;


class Embed extends Cplugin
{
    public function pluginChk($params)
    {
        return array(
            'status' => true,
            'params' => $params
        );
    }

    public function pluginConfig($params)
    {

        return array(
            'html' => $this->render('config', array('params' => $params)),
            'config' => true,
            'resizable' => false,
            'styles' => false
        );
    }

    public function pluginRender($params)
    {
        $return = '';
        $code = $params['code'];

        if (!empty($code)) {
            $code = htmlspecialchars_decode(htmlspecialchars_decode($code));
            $params['width'] = $this->getSize($params['width']);

            $code = preg_replace("~<iframe([^>]+)width=('|\")([0-9]+)('|\")([^>]+)>~", "<iframe \\1 width='100%' \\5>",$code);

            $return = $this->render('embed', [
                'code' => $code,
            ]);
        }
        return $return;
    }

}