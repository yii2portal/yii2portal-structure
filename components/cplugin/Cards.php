<?php

namespace yii2portal\structure\components\cplugin;

use yii2portal\cplugin\models\Cplugin;


class Cards extends Cplugin
{
    public static $titles = [];

    public function pluginChk($params)
    {
        return array(
            'status' => true,
            'params' => $params
        );
    }

    public function pluginConfig($params)
    {

        return array(
            'html' => $this->render('config', array('params' => $params)),
            'config' => true,
            'resizable' => false,
            'styles' => false
        );
    }

    public function pluginRender($params)
    {
        $return = '';
        $title = isset($params['title'])?$params['title']:'';
        $content = isset($params['content'])?$params['content']:'';

        if (!empty($title)) {
            $params['width'] = $this->getSize($params['width']);
            self::$titles[] = $title;

            $return = $this->render('cards', [
                'title' => $title,
                'content' => $content,
                'number' => count(self::$titles),
            ]);
        }
        return $return;
    }

    public static function getMenu(){

        return self::$titles;
    }
}