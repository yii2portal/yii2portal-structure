<?php

namespace yii2portal\structure\components\cplugin;

use yii2portal\cplugin\models\Cplugin;


class Lead extends Cplugin
{

    public function pluginChk($params)
    {
        return array(
            'status' => true,
            'params' => $params
        );
    }

    public function pluginConfig($params)
    {

        return array(
            'html' => $this->render('config', array('params' => $params)),
            'config' => true,
            'resizable' => false,
            'styles' => false
        );
    }

    public function pluginRender($params)
    {
        $return = '';



        $text = $params['title'];
        $mrg = isset($params['mrg'])?$params['mrg']:'';

        if (!empty($text)) {
            $params['width'] = $this->getSize($params['width']);
            $return = $this->render('lead', [
                'text' => htmlspecialchars_decode($text),
                'mrg' => $mrg
            ]);
        }


        return $return;
    }
}