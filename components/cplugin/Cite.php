<?php

namespace yii2portal\structure\components\cplugin;

use yii2portal\cplugin\models\Cplugin;


class Cite extends Cplugin
{

    public function pluginChk($params)
    {
        return array(
            'status' => true,
            'params' => $params
        );
    }

    public function pluginConfig($params)
    {

        return array(
            'html' => $this->render('config', array('params' => $params)),
            'config' => true,
            'resizable' => false,
            'styles' => false
        );
    }

    public function pluginRender($params)
    {
        $return = '';



        $text = isset($params['title'])?$params['title']:'';
        $author = isset($params['author'])?$params['author']:'';
        $mrg = isset($params['mrg'])?$params['mrg']:'';


        if (!empty($text)) {
            $params['width'] = $this->getSize($params['width']);
            $return = $this->render('cite', [
                'text' => htmlspecialchars_decode($text),
                'author' => htmlspecialchars_decode($author),
                'mrg' => $mrg
            ]);
        }


        return $return;
    }
}