<?php
namespace yii2portal\structure\controllers;

use Yii;
use yii2portal\core\controllers\Controller;

class IndexController extends Controller
{

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex($structure_id)
    {
        return $this->render('index',[
            'page'=>$this->module->getPage($structure_id)
        ]);
    }

    
}
