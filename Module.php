<?php

namespace yii2portal\structure;

use Yii;
use yii\base\BootstrapInterface;
use yii\helpers\ArrayHelper;
use yii\web\GroupUrlRule;
use yii2portal\structure\components\cplugin\Cards;
use yii2portal\structure\components\cplugin\Cite;
use yii2portal\structure\components\cplugin\Embed;
use yii2portal\structure\components\cplugin\Lead;
use yii2portal\structure\components\cplugin\Listtitles;
use yii2portal\structure\components\cplugin\Menutitles;
use yii2portal\structure\models\CoreStructure;

class Module extends \yii2portal\core\Module implements BootstrapInterface
{

    public $controllerNamespace = 'yii2portal\structure\controllers';

    private $_structure = [];

    /**
     * @var array GroupUrlRule[]
     */
    private $_groupRules = [];
    private $_urls = [];

    private function _makeUrl($elem, $parent)
    {
        if ($this->_structure[$elem]->pid > 0) {
            if (isset($this->_structure[$this->_structure[$elem]->pid])) {
                $tmp_url = $this->_makeUrl($this->_structure[$elem]->pid, $parent);
                $tmp = (!empty($tmp_url) ? $tmp_url . '/' : '') . $this->_structure[$elem]->url;
            } else
                $tmp = $this->_structure[$elem]->url;
            return $tmp;
        } else
            return '';
    }

    public function bootstrap($app)
    {
        Yii::$app->getModule('cplugin')->registerPlugin(Cite::className());
        Yii::$app->getModule('cplugin')->registerPlugin(Lead::className());
        Yii::$app->getModule('cplugin')->registerPlugin(Menutitles::className());
        Yii::$app->getModule('cplugin')->registerPlugin(Listtitles::className());
        Yii::$app->getModule('cplugin')->registerPlugin(Cards::className());
        Yii::$app->getModule('cplugin')->registerPlugin(Embed::className());

        $this->_structure = CoreStructure::find()->orderBy([
            'ord' => SORT_ASC,
            'level' => SORT_ASC,
        ])
            ->with('params')
            ->indexBy('id')
            ->all();


        foreach ($this->_structure as $s) {
            $moduleReal = $s->realModule;

            $url = $this->_makeUrl($s->id, $s->pid);
            $this->_urls[$s->id] = !empty($url) ? "/{$url}/" : "/";

            if (isset($app->modules[$moduleReal])) {


                $rulesPath = $app->getModule($moduleReal)->basePath . "/config/rules.php";
                $moduleRules = [];
                if (file_exists($rulesPath)) {
                    $configRules = require($rulesPath);

                    foreach ($configRules as $rule) {
                        $defaults = [
                            'structure_id' => $s->id,
                            'parent_id' => $s->pid,
                        ];

                        if (isset($rule['defaults'])) {
                            $defaults = ArrayHelper::merge($defaults, $rule['defaults']);
                        }

                        $moduleRules[] = [
                            'pattern' => "{$rule['pattern']}",
                            'name' => isset($rule['name']) ? $rule['name'] : null,
                            'route' => "{$rule['route']}",
                            'defaults' => $defaults
                        ];
                    }
                }


                $this->_groupRules[$s->id] = new GroupUrlRule([
                    'prefix' => "{$url}",
                    'routePrefix' => "$moduleReal",
                    'rules' => $moduleRules,
                ]);

                $app->urlManager->addRules([$this->_groupRules[$s->id]]);


            } else {
                Yii::info("page {$s->id} not loaded");
            }
        }
    }

    public function getCurrentPage()
    {
        $return = null;
        $structureId = Yii::$app->request->getQueryParam('structure_id');
        if (empty($structureId)) {
            list($structureId) = array_keys(array_filter($this->_urls, function ($url) {
                return $url == '/';
            }));
        }

        if (!empty($structureId)) {
            $return = $this->getPage($structureId);
        }

        return $return;
    }


    public function getPage($pageId)
    {
        if (isset($this->_structure[$pageId])) {
            return $this->_structure[$pageId];
        } else {
            return CoreStructure::findOne($pageId);
        }
    }


    /**
     * @param $moduleName
     * @return null|CoreStructure
     */
    public function getPageByModule($moduleName)
    {
        $return = null;

        $modules = $this->getPagesByModule($moduleName);

        if (!empty($modules)) {
            $return = array_shift($modules);
        }

        return $return;
    }

    /**
     * @param $moduleName
     * @return CoreStructure[]
     */
    public function getPagesByModule($moduleNames)
    {
        $return = [];

        if (!is_array($moduleNames)) {
            $moduleNames = [$moduleNames];
        }

        $modules = array_filter($this->_structure, function ($s) use ($moduleNames) {
            return in_array($s->module, $moduleNames);
        });

        return $modules;
    }

    public function getUrl($pageId)
    {
        if (isset($this->_urls[$pageId])) {
            return $this->_urls[$pageId];
        } else {
            return null;
        }
    }

    public function getUrlByModule($moduleName)
    {
        $return = null;

        if ($module = $this->getPageByModule($moduleName)) {
            $return = $this->getUrl($module->id);
        }

        return $return;
    }

    /**
     * @param $pageId
     * @return GroupUrlRule
     */
    public function getUrlRule($pageId)
    {
        if (isset($this->_groupRules[$pageId])) {
            return $this->_groupRules[$pageId];
        } else {
            return null;
        }
    }


    public function makeUrl($str)
    {
        $str = strip_tags($str);
        $tr = array(
            "А" => "a", "Б" => "b", "В" => "v", "Г" => "g",
            "Д" => "d", "Е" => "e", "Ё" => "e", "Ж" => "j", "З" => "z", "И" => "i",
            "Й" => "y", "К" => "k", "Л" => "l", "М" => "m", "Н" => "n",
            "О" => "o", "П" => "p", "Р" => "r", "С" => "s", "Т" => "t",
            "У" => "u", "Ф" => "f", "Х" => "h", "Ц" => "ts", "Ч" => "ch",
            "Ш" => "sh", "Щ" => "sch", "Ъ" => "", "Ы" => "yi", "Ь" => "",
            "Э" => "e", "Ю" => "yu", "Я" => "ya", "а" => "a", "б" => "b",
            "в" => "v", "г" => "g", "д" => "d", "е" => "e", "ё" => "e", "ж" => "j",
            "з" => "z", "и" => "i", "й" => "y", "к" => "k", "л" => "l",
            "м" => "m", "н" => "n", "о" => "o", "п" => "p", "р" => "r",
            "с" => "s", "т" => "t", "у" => "u", "ф" => "f", "х" => "h",
            "ц" => "ts", "ч" => "ch", "ш" => "sh", "щ" => "sch", "ъ" => "y",
            "ы" => "yi", "ь" => "", "э" => "e", "ю" => "yu", "я" => "ya",
            " " => "_", "." => "", "/" => "_", 'ө' => 'o', 'ү' => 'u', 'ң' => 'n', 'Ү' => 'u', 'Ң' => 'n', 'Ө' => 'o'
        );
        $str = strtr($str, $tr);
        return preg_replace('/[^A-Za-z0-9_\-]/', '', $str);
    }


}