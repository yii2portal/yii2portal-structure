Yii2Portal structure
====================
Bse module for yii2portal

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist yii2portal/yii2portal-structure "*"
```

or add

```
"yii2portal/yii2portal-structure": "*"
```

to the require section of your `composer.json` file.


Usage
-----

Once the extension is installed, simply use it in your code by  :

```php
<?= \yii2portal\structure\AutoloadExample::widget(); ?>```
